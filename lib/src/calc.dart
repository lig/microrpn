import 'package:flutter/material.dart';

import 'views.dart';

class Calc extends StatelessWidget {
  const Calc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: KeyboardController(
        child: Container(
          constraints: const BoxConstraints.expand(),
          child: Column(
            children: const [
              DisplayView(),
              KeyboardView(),
            ],
          ),
        ),
      ),
    );
  }
}
