import 'dart:collection';
import 'dart:developer' as dev;
import 'dart:math';

import 'package:flutter/foundation.dart';

import '../actions.dart';

extension Stack<E> on ListQueue<E> {
  void push(E value) {
    addFirst(value);
  }

  E pop() => removeFirst();
}

class StackModel extends ChangeNotifier {
  String? _current;
  final ListQueue<double> _stack = ListQueue();

  int get length => _stack.length;
  List<double> get stack => List.from(_stack);

  String? get current => _current;
  set current(String? value) {
    _current = value;
    notifyListeners();
  }

  void perform(ActionDef action) {
    if (action is InputAction) {
      _performInput(action.value);
    } else if (action is SpecialAction) {
      _performSpecial(action.id);
    } else if (action is UnaryOperation) {
      if (!_pushCurrent()) return;
      _performUnaryOp(action.id);
    } else if (action is BinaryOperation) {
      if (!_pushCurrent()) return;
      _performBinaryOp(action.id);
    } else {
      dev.log("Unknown action: `$action`");
    }
  }

  void _performInput(String character) {
    String current = _current ?? "";

    if (character == ",") {
      character = ".";
    }

    if (character == "-") {
      if (current.startsWith("-")) {
        _current = current.substring(1);
        notifyListeners();
        return;
      }
      if (current == "") {
        current = "0";
      }
      _current = character + current;
      notifyListeners();
      return;
    }

    if (character == ".") {
      if (current.contains(".")) {
        return;
      }
      if (current == "") {
        current = "0";
      }
    } else if (current == "0") {
      current = "";
    } else if (current == "-0") {
      current = "-";
    }

    _current = current + character;
    notifyListeners();
  }

  void _performSpecial(SpecialActionId id) {
    switch (id) {
      case SpecialActionId.enter:
        _pushCurrent();
        break;
      case SpecialActionId.drop:
        _drop();
        break;
      case SpecialActionId.swap:
        _swap();
        break;
    }
  }

  bool _pushCurrent() {
    String? current = _current;
    if (current == null) return true;

    double? value = double.tryParse(current);
    if (value == null) return false;

    _current = null;
    _stack.push(value);
    notifyListeners();
    return true;
  }

  void _drop() {
    if (_stack.isEmpty) return;
    _stack.pop();
    notifyListeners();
  }

  void _swap() {
    if (_stack.length < 2) return;
    double first = _stack.pop();
    double second = _stack.pop();
    _stack.push(first);
    _stack.push(second);
    notifyListeners();
  }

  void _performUnaryOp(UnaryOperationId id) {
    if (_stack.isEmpty) {
      return;
    }

    double x = _stack.pop();
    double result;

    switch (id) {
      case UnaryOperationId.sqrt:
        result = sqrt(x);
        break;
    }

    _stack.push(result);
    notifyListeners();
  }

  void _performBinaryOp(BinaryOperationId id) {
    if (_stack.length < 2) {
      return;
    }

    double x = _stack.pop();
    double y = _stack.pop();
    double result;

    switch (id) {
      case BinaryOperationId.add:
        result = y + x;
        break;
      case BinaryOperationId.substract:
        result = y - x;
        break;
      case BinaryOperationId.multiply:
        result = y * x;
        break;
      case BinaryOperationId.divide:
        result = y / x;
        break;
    }

    _stack.push(result);
    notifyListeners();
  }
}
