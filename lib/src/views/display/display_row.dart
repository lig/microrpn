import 'package:flutter/material.dart';

import 'package:dotted_decoration/dotted_decoration.dart';

import 'constants.dart';

class DisplayRowView extends StatelessWidget {
  final String label;
  final String value;

  const DisplayRowView({
    Key? key,
    required this.label,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: DottedDecoration(
        color: Theme.of(context).primaryColor.withAlpha(127),
        dash: const [2, 2],
        strokeWidth: displayRowUnderlineWidth,
      ),
      padding: const EdgeInsets.fromLTRB(
          0, displayRowPaddingTop, 0, displayRowPaddingBottom),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "$label:",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          Text(
            value.endsWith(".0") ? value.substring(0, value.length - 2) : value,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ],
      ),
    );
  }
}
