const double displayFontSize = 18.0;
const double displayLetterSpacing = 1.0;
const double displayRowPaddingBottom = 2.0;
const double displayRowPaddingTop = 6.0;
const double displayRowUnderlineWidth = 1.0;
