import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../models.dart';
import 'display_row.dart';

class StackView extends StatelessWidget {
  const StackView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<StackModel, List<double>>(
      selector: (context, model) => model.stack,
      builder: (context, stack, child) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        verticalDirection: VerticalDirection.up,
        children: List<DisplayRowView>.generate(
          stack.length,
          (index) {
            int label = index + 1;
            return DisplayRowView(
              label: "$label",
              value: "${stack[index]}",
            );
          },
        ),
      ),
    );
  }
}
