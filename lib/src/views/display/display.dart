import 'package:flutter/material.dart';

import 'constants.dart';
import 'input.dart';
import 'stack.dart';

class DisplayView extends StatelessWidget {
  const DisplayView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        backgroundColor: const Color(0xffd7d2cc),
        primaryColor: Colors.black,
        textTheme: Theme.of(context).textTheme.copyWith(
              bodyText2: Theme.of(context).textTheme.bodyText2?.copyWith(
                    color: Colors.black,
                    fontFamily: "DSEG14Modern",
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    fontSize: displayFontSize,
                    letterSpacing: displayLetterSpacing,
                  ),
            ),
      ),
      child: Builder(builder: (context) {
        return Expanded(
          child: Container(
            color: Theme.of(context).backgroundColor,
            child: Wrap(
              verticalDirection: VerticalDirection.up,
              clipBehavior: Clip.hardEdge,
              children: const [
                InputView(),
                StackView(),
              ],
            ),
          ),
        );
      }),
    );
  }
}
