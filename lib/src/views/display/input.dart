import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../models.dart';
import 'display_row.dart';

class InputView extends StatelessWidget {
  const InputView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<StackModel, String?>(
      selector: (context, stackModel) => stackModel.current,
      shouldRebuild: (previous, next) => previous != next,
      builder: (context, current, child) => current == null
          ? Container()
          : DisplayRowView(label: "-", value: current),
    );
  }
}
