import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:spannable_grid/spannable_grid.dart';

import '../../actions.dart';
import '../../models.dart';
import 'calc_button.dart';

class KeyboardView extends StatelessWidget {
  const KeyboardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: SpannableGrid(
        editingStrategy: SpannableGridEditingStrategy.disabled(),
        columns: 4,
        rows: 5,
        style: const SpannableGridStyle(spacing: 2.0),
        emptyCellView: Container(),
        cells: actionRegistry.buttons.map((action) {
          final StackModel model = context.read();

          final ButtonDef button = action.button!;
          final int rowSpan;
          switch (button.size) {
            case ButtonSize.normal:
              rowSpan = 1;
              break;
            case ButtonSize.double:
              rowSpan = 2;
              break;
          }

          return SpannableGridCellData(
            id: action.id,
            row: button.row,
            column: button.column,
            rowSpan: rowSpan,
            child: CalcButton(
              label: button.label,
              onPressed: () => model.perform(action.def),
            ),
          );
        }).toList(),
      ),
    );
  }
}
