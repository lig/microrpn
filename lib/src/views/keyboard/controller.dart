import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:provider/provider.dart';

import '../../actions.dart';
import '../../models.dart';

class KeyboardController extends StatelessWidget {
  final Widget child;

  const KeyboardController({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => FocusNode(),
      builder: (context, child) => KeyboardListener(
        focusNode: context.read(),
        autofocus: true,
        onKeyEvent: (keyEvent) {
          log("${keyEvent.character} / ${keyEvent.logicalKey.keyLabel} / ${keyEvent.logicalKey.synonyms}");
          if (keyEvent is! KeyDownEvent) return;

          StackModel model = context.read();

          String key = (keyEvent.character ?? "").trim() != ""
              ? keyEvent.character!.toUpperCase()
              : keyEvent.logicalKey.keyLabel;

          ActionDef? action = actionRegistry.opDefByKey[key];

          if (action == null) return;

          model.perform(action);
        },
        child: child!,
      ),
      child: child,
    );
  }
}
