import 'dart:collection';

class ActionRegistry {
  final List<Action> ops;
  final HashMap<String, ActionDef> opDefByKey;
  final List<Action> buttons;

  ActionRegistry(this.ops)
      : opDefByKey = ops
            .where((op) => op.key != null)
            .map((op) => [
                  MapEntry(op.key!, op.def),
                  ...op.keyAliases.map((keyAlias) => MapEntry(keyAlias, op.def))
                ])
            .fold(HashMap<String, ActionDef>(),
                (value, entryList) => value..addEntries(entryList)),
        buttons = [...ops.where((op) => op.button != null)];
}

class Action {
  final String id;
  final ActionDef def;
  final ButtonDef? button;
  final String? key;
  final List<String> keyAliases;

  Action({
    ActionDef? def,
    this.button,
    this.key,
    List<String>? keyAliases,
  })  : assert(button != null || key != null),
        assert(keyAliases == null || key != null),
        id = (button?.label ?? key)!,
        def = def ?? InputAction(value: (key ?? button?.label)!),
        keyAliases = keyAliases ?? [];
}

abstract class ActionDef {}

class InputAction implements ActionDef {
  final String value;

  InputAction({
    required this.value,
  });
}

class SpecialAction implements ActionDef {
  final SpecialActionId id;

  SpecialAction({
    required this.id,
  });
}

enum SpecialActionId {
  enter,
  swap,
  drop,
}

class UnaryOperation implements ActionDef {
  final UnaryOperationId id;

  UnaryOperation({
    required this.id,
  });
}

enum UnaryOperationId {
  sqrt,
}

class BinaryOperation implements ActionDef {
  final BinaryOperationId id;

  BinaryOperation({
    required this.id,
  });
}

enum BinaryOperationId {
  add,
  substract,
  multiply,
  divide,
}

class ButtonDef {
  final String label;
  final int row;
  final int column;
  final ButtonSize size;

  ButtonDef({
    required this.label,
    required this.row,
    required this.column,
    this.size = ButtonSize.normal,
  });
}

enum ButtonSize { normal, double }
