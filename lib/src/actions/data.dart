import 'base.dart';

final ActionRegistry actionRegistry = ActionRegistry([
  Action(
    button: ButtonDef(label: "0", row: 5, column: 1),
    key: "0",
  ),
  Action(
    button: ButtonDef(label: "1", row: 4, column: 1),
    key: "1",
  ),
  Action(
    button: ButtonDef(label: "2", row: 4, column: 2),
    key: "2",
  ),
  Action(
    button: ButtonDef(label: "3", row: 4, column: 3),
    key: "3",
  ),
  Action(
    button: ButtonDef(label: "4", row: 3, column: 1),
    key: "4",
  ),
  Action(
    button: ButtonDef(label: "5", row: 3, column: 2),
    key: "5",
  ),
  Action(
    button: ButtonDef(label: "6", row: 3, column: 3),
    key: "6",
  ),
  Action(
    button: ButtonDef(label: "7", row: 2, column: 1),
    key: "7",
  ),
  Action(
    button: ButtonDef(label: "8", row: 2, column: 2),
    key: "8",
  ),
  Action(
    button: ButtonDef(label: "9", row: 2, column: 3),
    key: "9",
  ),
  Action(
    def: InputAction(value: "-"),
    button: ButtonDef(label: "±", row: 5, column: 2),
    key: "F9",
  ),
  Action(
    button: ButtonDef(label: ".", row: 5, column: 3),
    key: ".",
  ),
  Action(
    def: SpecialAction(id: SpecialActionId.enter),
    button: ButtonDef(label: "⏎", row: 4, column: 4, size: ButtonSize.double),
    key: "Enter",
    keyAliases: ["Numpad Enter"],
  ),
  Action(
    def: BinaryOperation(id: BinaryOperationId.add),
    button: ButtonDef(label: "+", row: 2, column: 4, size: ButtonSize.double),
    key: "+",
  ),
  Action(
    def: BinaryOperation(id: BinaryOperationId.substract),
    button: ButtonDef(label: "-", row: 1, column: 4),
    key: "-",
  ),
  Action(
    def: BinaryOperation(id: BinaryOperationId.multiply),
    button: ButtonDef(label: "*", row: 1, column: 3),
    key: "*",
  ),
  Action(
    def: BinaryOperation(id: BinaryOperationId.divide),
    button: ButtonDef(label: "/", row: 1, column: 2),
    key: "/",
  ),
  Action(
    def: UnaryOperation(id: UnaryOperationId.sqrt),
    button: ButtonDef(label: "√", row: 1, column: 1),
    key: "R",
  ),
  Action(
    def: SpecialAction(id: SpecialActionId.drop),
    key: "D",
  ),
  Action(
    def: SpecialAction(id: SpecialActionId.swap),
    key: "S",
  ),
]);
