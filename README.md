# MicroRPN

MicroRPN is an RPN calculator.

The project aims to develop a multiplatform RPN calculator.

MicroRPN is in an early stage of development.

Currently, the development focuses on a Linux version of the app.

MicroRPN is written in Dart/Flutter and licensed under GPL v3 license.

![MicroRPN WIP Screenshot](screenshot.png)


## What works

- Entering numbers (no backspace yet).
- Operations for all buttons including physical keyboard (use "R" for `√`).
- Use "D" for drop.
- Use "S" for swap.


## How to contribute

Any contributions are welcome.

Use the [Gitlab Issues](https://gitlab.com/lig/microrpn/-/issues) to communicate.
